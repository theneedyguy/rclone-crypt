FROM golang:1.13.5 as builder
RUN go get -d github.com/rfjakob/gocryptfs && \
cd $(go env GOPATH)/src/github.com/rfjakob/gocryptfs && \
./build-without-openssl.bash

FROM alpine:latest
RUN apk --no-cache add fuse bash ca-certificates sudo curl
RUN mkdir -p /crypt /plain /etc/rclone/config
RUN curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip && \
unzip rclone-current-linux-amd64.zip && \
cd rclone-*-linux-amd64 && \
sudo cp rclone /usr/bin/ && \
sudo chown root:root /usr/bin/rclone && \
sudo chmod 755 /usr/bin/rclone
COPY --from=builder /go/bin/gocryptfs /bin
RUN mkdir -p /root/.config/rclone
VOLUME /plain /crypt root/.config/rclone

WORKDIR /root
CMD /bin/bash
